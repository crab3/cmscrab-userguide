# FAQ

## Where is Rucio documentation?

- The main [CMS Rucio documentation](https://twiki.cern.ch/twiki/bin/viewauth/CMS/Rucio). Recommend reading the "Rosetta stone", terminology different between Rucio and CMS world.
- [The official Rucio Documentation](https://rucio.cern.ch/documentation/).
- For Rucio CLI, `rucio --help` to list all commands and `rucio <command> --help` to see how to use.

## How do I request the quota?

Please see [CMS Rucio documentation][https://twiki.cern.ch/twiki/bin/view/CMSPublic/RucioUserDocsQuotas#Requesting_a_quota]

If that is not sufficient for your needs, or you have problems, you may have to
contact the Data Management team. In this alpha stage, please send an email to us (<cms-service-crab-operators@cern.ch>) directly.


## My Rule is "STUCK".

If the rule is "STUCK", check the reason field on the Rule page to see why it is stuck.
Feel free to ask in [CMSTalk's Computing Tools](https://cms-talk.web.cern.ch/c/offcomp/comptools/87)
if you need help understanding what does it means.
By far, the most common causes of STUCK transfers are:

1. full quota at destination (for you to fix ! `crab status` will warn you of this situation)
2. your destination site has a problem (down ? misconfiguration ?), this is your "home T2", you can certainly contact local admins
3. the site where jobs ran, which holds the temporary replica, has a problem. Hopefully will be solved by waiting. 

## Locks keep in "REPLICATING" state.

It is unusual for a lock to get stuck in `REPLICATING` longer than a day without changing to another state (like `STUCK` and retrying again).
Note that a lock (file) my go from REPLICATING to STUCK and back to REPLICATING in e.g. 24h, so be careful before calling wolf here !
If you have evidence of a problem please report it to the CRAB Operator in [CMSTalk's Computing Tools channel](https://cms-talk.web.cern.ch/c/offcomp/comptools/87) if you see it.


## How do I delete files (or free my Rucio Quota)?

In Rucio Stageout, you cannot delete each file manually because files are now owned by Rucio account. What you can do is delete the Replication Rule.

To delete a rule, you can use Rucio Web UI's [R2D2 page](https://cms-rucio-webui.cern.ch/r2d2), click the rule you want to delete, and click "Delete rule(s)" button at the buttom of the page.

Or you can use Rucio CLI command:

```bash
rucio delete-rule <ruleid>
```

Note:

- Rucio has a deletion policy of 1 hour of grace period if you change your mind.
- Rucio might not delete files immediately. But you will get the quota back to your account.

<!--
TODO: lookup the proper command and checks if user can kick SUSPEND rule back by themselves
--->
